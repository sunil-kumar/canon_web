<?php include 'register-header.php'; ?>
<section class="enteries">
    <div class="container-fluid">
        <div class="col-md-12">
        <div class="grid-view">
            <img src="assets/images/grid.svg" alt="" class="svg grid"/>           
        </div>
        </div>
        <div class="col-md-12">
            <div class="vote-popup">
            <div class="thank-msg">
               <img src="assets/images/close.svg" class="svg thank-close">
                <div class="clearfix"></div>
                <p>Thanks for liking.<br/>Please come back tomorrow <br/>to vote again. </p>
            </div>
        </div>
        </div>
    </div>
    
</section>
<?php include 'enteries-footer.php'; ?>