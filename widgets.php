<?php include 'header.php'; ?>
<section class="widget">
    <div class="container-fluid">
        <div class="row">
            <div class="container-tab">
            <div class="">

                <div class="tab-panel">
  <!-- Nav tabs -->
  <ul class="nav nav-tabs nav-justified" role="tablist">
    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Flight Reservation</a></li>
    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">MY BOOKING</a></li>
    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab" class="border-r" >FLIGHT STATUS</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
      <div role="tabpanel" class="tab-pane fade in active" id="home">
          <div class="tab-top">
          <div class="tab-checkbox round-w">
              <label>
              <div class="squaredTwo">                
                    <input type="checkbox" value="None" id="squaredTwo" name="check" />
                    <label for="squaredTwo"></label>   
                    </div>
                    <span>ROUND TRIP  </span>  
              </label>
          </div>
          <div class="tab-checkbox one-way">
              <label>
              <div class="squaredTwo">                
                    <input type="checkbox" value="None" id="squaredTwo" name="check" />
                    <label for="squaredTwo"></label>   
                    </div>
                    <span>ONE WAY</span>  
              </label>
          </div>
          <div class="tab-checkbox">
              <label>
                  <img src="assets/images/plane-out.svg" class="svg plane-out" alt=""/>            
                    <span>Multiple cities</span>  
              </label>
          </div>
      </div>
          <div class="tab-bottom">
              <form>
                  <div class="col-md-12">
                      <div class="row">
                  <div class="form-group">
                      <select class="form-control tab-select ">
                          <option value="">From</option>
                      </select>
                  </div>
                   <div class="form-group">
                      <select class="form-control tab-select ">
                          <option value="">To</option>
                      </select>
                  </div>
                      </div>
                  </div>
                  
                 <div class="container-fluid">
                       <div class="row">
                  <div class="calander-content">
                     
                      <div class="input-group">
                            <input type="text" class="form-control calander-txt" placeholder="depart" aria-describedby="basic-addon2">
                            <span class="input-group-addon" id="basic-addon2"><img src="assets/images/calander_icon.png" class="calander" alt=""/></span>
                        </div>
                          </div>
                 
                  <div class="calander-content ml">
                      <div class="input-group">
                            <input type="text" class="form-control calander-txt" placeholder="return" aria-describedby="basic-addon2">
                            <span class="input-group-addon" id="basic-addon2"><img src="assets/images/calander_icon.png" class="calander" alt=""/></span>
                        </div>
                  </div>
                  </div>
                       </div>
                  <div class="clearfix"></div>
                  <div class="container-fluid">
                       <div class="row">
                  <div class="calander-content">
                     
                      <div class="form-group">
                      <select class="form-control tab-select">
                          <option value="">ADULT</option>
                      </select>
                  </div>
                          </div>
                 
                  <div class="calander-content ml">
                      <div class="form-group">
                      <select class="form-control tab-select">
                          <option value="">CHILD</option>
                      </select>
                  </div>
                  </div>
                  </div>
                       </div>
              </form>
          </div>
      </div>
    <div role="tabpanel" class="tab-pane fade" id="profile">...</div>
    <div role="tabpanel" class="tab-pane fade" id="messages">...</div>
  </div>
</div> 
            </div>
 </div> 
        </div>
</div> 
</section>



<?php include 'footer.php'; ?>