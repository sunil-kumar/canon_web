<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="assets/css/style.css">
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
<script type="text/javascript" src="assets/js/jquery-1.12.3.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.js"></script>
<script type="text/javascript" src="assets/js/custom.js"></script>
<section class="header-top">
    <div class="container-fluid">
        <div class="row">
            <div class="top-menubar">
            <div class="col-xs-3 col-sm-2 col-md-1" >
                <div class="logo">
                    <img src="assets/images/canon_logo.svg" class="svg logo-img">
                   
                </div>
            </div>
                <div class="col-xs-12  col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-5  ">
                    <div class="menu">
                        <ul class="list-inline">
                            <li><a href='http://amsytclients.com/canon/rules.php'>RULES</a></li>
                            <li><a href=''>CAPTURE</a></li>
                            <li><a href='http://amsytclients.com/canon/entries-view.php'>ENTRIES</a></li>
                            <li><a href='http://amsytclients.com/canon/eos-80d.php'>EOS 80D</a></li>
                            <li><a href='http://amsytclients.com/canon/winners.php'>WINNERS</a></li>
                        </ul>
                    </div>
                </div>
        </div>
        </div>
    </div>
</section>