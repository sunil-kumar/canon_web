<?php include 'header.php'; ?>
<section class="eos">
    <div class="eos-bg"> </div>
    <div class="container">
        <div class="row">
            <div class="eos-slider">
                <div class="col-md-4 col-sm-5 col-lg-4">
                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
 
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
        <img src="assets/images/eos80d.png" alt=""/>
    </div>

    <div class="item">
      <img src="assets/images/eos80d.png" alt=""/>
    </div>

    <div class="item">
     <img src="assets/images/eos80d.png" alt=""/>
    </div>

  </div>
 </div>
                </div>
                <div class="col-md-8 col-sm-7 col-lg-8">
                    <div class="eos-detail">
                        <h1>EOS 80D</h1>
                        <p>The EOS 80D&acute;s 24.2MP, APS-C CMOS sensor realizes high<br/> resolution photo quality, 
                            where landscapes and still life shots are<br/> rendered with even greater detail, and skin 
                            tones and textures<br/> with even better precision. The tremendous volume of data<br/> generated 
                            undergoes high speed processing via the DIGIC 6<br/> processor, enabling speeds of up to 7.0fps 
                            for photos (continuous<br/> shooting) and Full HD 60p/50p for movies to be achieved.                         
                        </p>
                        <div class="eos-btn-group">
                               
                                    <a class="btn btn-default mr-r">more info</a>
                                     <a class="btn btn-default">buy now</a>
                              
                        </div>

                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            
        </div>
    </div>
    
</section>

<?php include 'footer.php'; ?>