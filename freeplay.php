<?php include 'register-header.php'; ?>

<section class="submission">
    <div class="container-fluid">
        <div class="row">
           
            <div class="proposal-container freeplay-con" id="freeplay-msg">
                    <h4>Oops!<br/>  You&acute;ve exceeded the <br/> submission limit</h4>
                    <p class="t-msg">Feel free to practice using the EOS 80D</p>
                    <div class="group-btn">                    
                     <a class="btn btn-default continue-btn">Continue</a>
                </div>
                </div>            
        </div>
    </div>
    
</section>


<?php include 'footer.php'; ?>