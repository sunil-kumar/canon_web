
<div class="container-fluid">
    <div class="col-md-12">
        <div class="like-footer">
            <ul class="list-inline share-book">
                <li>
                    <i id="empty-heart"><img src="assets/images/heart.svg" alt="" class="svg heart"/></i>
                    <i id="fill-heart"><img src="assets/images/heart.png" alt="" class="heart"/></i>
                    
                    <span>1345</span></li>
                <li><img src="assets/images/share.svg" alt="" class="svg share"/></li>
            </ul>
             <div class="clearfix"></div>
        <div class="copyright">
                <div class="col-sm-6 col-xs-12">
                    <ul class="list-inline text-left">
                        <li><a href="http://www.canon.co.in/personal/web/terms" target="_blank">Terms of Use</a></li>
                        <li><a href="http://www.canon.co.in/personal/web/privacy" target="_blank">Privacy Policy</a></li>
                    </ul>
                </div>
                <div class="col-sm-6 col-xs-12 text-right copy-text">
                    <span>Copyright &copy; 2016 Canon India Pvt Ltd. All Rights Reserved</span>
                </div>
                 <div class="clearfix"></div>
            </div>
    </div>
        </div>
    </div>
   
</div
