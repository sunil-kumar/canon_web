<?php include 'register-header.php'; ?>
<section class="winner-section">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
    <li data-target="#myCarousel" data-slide-to="3"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
        <img src="assets/images/images/image_3_big.png" alt=""/>
         <div class="container-fluid">
            <div class="winner-testimonial">
                <h1>GRAND PRIZE <br/>WINNER</h1>  
                <h4>Slackers</h4>
                <p>Found these guys<br/> slacking on the job<br/> and thought it was<br/> funny. The EOS 80D<br/> will help me decide<br/>
                    if I&acute;m ready for the<br/> next level camera<br/> from Canon. <p>
                <p><img src="assets/images/images/testimonial.png" alt=""/>
                    <span>Yolanda P</span></p>
            </div>
        </div>
    </div>

    <div class="item">
       <img src="assets/images/images/image_3_big.png" alt=""/>
         <div class="container-fluid">
            <div class="winner-testimonial">
                <h1>GRAND PRIZE <br/>WINNER</h1>  
                <h4>Slackers</h4>
                <p>Found these guys<br/> slacking on the job<br/> and thought it was<br/> funny. The EOS 80D<br/> will help me decide<br/>
                    if I&acute;m ready for the<br/> next level camera<br/> from Canon. <p>
                <p><img src="assets/images/images/testimonial.png" alt=""/>
                    <span>Yolanda P</span></p>
            </div>
        </div>
    </div>

    <div class="item">
       <img src="assets/images/images/image_3_big.png" alt=""/>
         <div class="container-fluid">
            <div class="winner-testimonial">
                <h1>GRAND PRIZE <br/>WINNER</h1>  
                <h4>Slackers</h4>
                <p>Found these guys<br/> slacking on the job<br/> and thought it was<br/> funny. The EOS 80D<br/> will help me decide<br/>
                    if I&acute;m ready for the<br/> next level camera<br/> from Canon. <p>
                <p><img src="assets/images/images/testimonial.png" alt=""/>
                    <span>Yolanda P</span></p>
            </div>
        </div>
    </div>

    <div class="item">
       <img src="assets/images/images/image_3_big.png" alt=""/>
        <div class="container-fluid">
            <div class="winner-testimonial">
                <h1>GRAND PRIZE <br/>WINNER</h1>  
                <h4>Slackers</h4>
                <p>Found these guys<br/> slacking on the job<br/> and thought it was<br/> funny. The EOS 80D<br/> will help me decide<br/>
                    if I&acute;m ready for the<br/> next level camera<br/> from Canon. <p>
                <p><img src="assets/images/images/testimonial.png" alt=""/>
                    <span>Yolanda P</span></p>
            </div>
        </div>
    </div>
  </div>
    </div>
    <div class="winner-footer">
    <a href="" class="f-share"><i class="fa fa-facebook"></i>Share</a>  
    <div class="clearfix"></div>
        <div class="copyright">
                <div class="col-sm-6">
                    <ul class="list-inline text-left">
                        <li><a href="http://www.canon.co.in/personal/web/terms" target="_blank">Terms of Use</a></li>
                        <li><a href="http://www.canon.co.in/personal/web/privacy" target="_blank">Privacy Policy</a></li>
                    </ul>
                </div>
                <div class="col-md-6 text-right">
                    <span>Copyright &copy; 2016 Canon India Pvt Ltd. All Rights Reserved</span>
                </div>
                 <div class="clearfix"></div>
            </div>
    </div>
</section>