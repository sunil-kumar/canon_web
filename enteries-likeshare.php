<?php include 'register-header.php'; ?>


<section class="enteries">
    <div class="container-fluid">
        <div class="col-md-12">
        <div class="grid-view">
            <img src="assets/images/grid.svg" alt="" class="svg grid"/>           
        </div>
        </div>
        <div class="col-md-12">
            <div class="entry-msg"  id="entry-msg">
                <h4>Slackers</h4>
                <p>Found these guys<br/> slacking on the job<br/> and thought it was<br/> funny.
                    The EOS 80D<br/> will help me decide<br/> if I&acute;m ready for the<br/> next level camera<br/> from Canon. </p>
            </div>
        </div>
    </div>
    
    <div class="thank-msg" id="vote-msg">
               <img src="assets/images/close.svg" class="svg thank-close">
                <div class="clearfix"></div>
                <p>Thanks for liking.<br/>Please come back tomorrow <br/>to vote again. </p>
            </div>
    
</section>
<script>

$(document).ready(function() {
 $('#empty-heart').click(function(){ 
   $("#empty-heart").hide();
   $("#entry-msg").hide();
   $("#fill-heart").show();
    $("#vote-msg").show();
   return false;
 });
});

</script>


<?php include 'enteries-footer.php'; ?>