<?php include 'register-header.php'; ?>
<link rel="stylesheet" type="text/css" href="assets/css/nanoscroller.css"/>
<link rel="stylesheet" type="text/css" href="assets/css/main.css"/>
<script>
$(function(){

  $('.nano').nanoScroller({
    preventPageScrolling: true
  });
  $("#main").find('.description').load("readme.html", function(){
    $(".nano").nanoScroller();
    $("#main").find("img").load(function() {
        $(".nano").nanoScroller();
    });
  });


});

</script>

<section class="rule-section">
    <div class="rule-bg"></div>
    <div id="main">        
    <div class="rules">
        <div class="container-fluid">
        <div class="col-md-12 rule-padding">
         <img src="assets/images/close.svg" class="svg rule-btn">
          <ul class="list-unstyled">
                    <li>
                        <h4>Contest Rules</h4>
                        <p>Follow these simple instructions and join the EOS 80D Photo Contest today.</p>
                    </li>
          </ul>
        </div>
        </div>
                <div class="container-fluid">
        <div class="col-md-12 rule-padding">
            <div class="fix-rule scroll-height">
                <div class="nano">
      <div class="overthrow nano-content description">
           
                <ul class="list-unstyled">
                    
                    <li>
                        <h4>Step 1</h4>
                        <p>Register using your Facebook or E-mail</p>
                    </li>
                     <li>
                        <h4>Step 2</h4>
                        <p>Travel the world using Google streetview & the EOS 80D to capture your best photos.</p>
                    </li>
                     <li>
                        <h4>Step 3</h4>
                        <p>Submit your photos</p>
                    </li>
                     <li>
                        <h4>Step 4</h4>
                        <p>Like and Share your favourite photos.</p>
                    </li>
                    <li>                   
                        <p>
                            Judging criteria is based on 50% public vote and 50% Canon/ANA<br/>
                            Participants are allowed to 3 submissions per day
                        </p>
                    </li>
                    <li class="margin-b">
                        <h3>TermS & ConditionS</h3>
                    </li>
                    
                    
                    
                </ul>
            </div>
                    </div>
                </div>
            </div> 
           </div> 
        </div>
    </div>
</section>
<script src="assets/js/jquery.nanoscroller.js"></script>
<script src="assets/js/main.js"></script>


<?php include 'footer.php'; ?>