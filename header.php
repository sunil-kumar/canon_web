<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="assets/css/style.css">
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
<script type="text/javascript" src="assets/js/jquery-1.12.3.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.js"></script>
<script type="text/javascript" src="assets/js/custom.js"></script>
<script type="text/javascript">
$(function()
{
    var url = window.location.href;
    $("#navbar-collapse-1 ul li a").each(function() 
    {
        if(url == (this.href)) { 
            $(this).closest("a").addClass("active");
        }
    });
})
</script>
<header>
    <div class="container-fluid">
        <div class="row">
            <div class="top-header">
            <div class="col-xs-4 col-sm-2 col-md-1" >
                <div class="logo">
                    <img src="assets/images/canon_logo.svg" class="svg logo-img">
                   
                </div>
            </div>
                <div class="col-xs-offset-2 col-xs-6 col-sm-2 col-md-2 col-sm-offset-8 col-md-offset-9">
                    <div class="menu-icon">
                        <button type="button" class="navbar-toggle toggle-display">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
                    </div>
                </div>
                </div>
        </div>
        </div>
    </div>
</header>