<?php include 'gameplay-header.php'; ?>




<section class="instruction">
    <!-----popup ------>
    <div class="popup-bg" id="popup-bg">

        <div class="popup">
            <div class="container">
                <div class="row">
                    <span id="instruction-con"><img src="assets/images/close.svg" class="svg close-btn cross-btn small-cross" alt="" /></span>
                    <div class="col-md-4 col-sm-12">
                        <div class="arrow-btn-con">
                            <div class="left-btn">
                                <button type="button" class="btn btn-default arrow-btn"><i class="fa fa-caret-left"></i></button>
                            </div>
                            <div class="left-btn">
                                <button type="button" class="btn btn-default arrow-top"><i class="fa fa-caret-up"></i></button>
                                <button type="button" class="btn btn-default arrow-btn arrow-bottom"><i class="fa fa-caret-down"></i></button>
                            </div>
                            <div class="left-btn">
                                <button type="button" class="btn btn-default arrow-btn"><i class="fa fa-caret-right"></i></button>
                            </div>
                        </div>
                        <p class="btn-text">Press arrows to navigate</p>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <div class="enter-con">
                            <div class="enter-return">
                                <div class="form-group">
                                    <input type="text" class="enter-txt" placeholder="enter">
                                    <a href="" class="return">return</a>
                                </div> 
                            </div>
                            <p class="btn-text">Press enter to capture</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <div class="btn-con">
                            <div class="b-group">
                                <button type="button" class="btn btn-default"><i class="fa fa-plus"></i></button>
                                <button type="button" class="btn btn-default"><i class="fa fa-minus"></i></button>
                            </div>                        
                        </div>
                        <p class="btn-text">Press minus/plus to zoom</p>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- popup--->
    <div class="ins-popup" id="eos-popup">
        <span id="eos-close"><img src="assets/images/close.svg" class="svg close-btn"/></span>
        <div class="clearfix"></div>
        <h3>Did you know the EOS 80D<br>  is equipped with &ndash;</h3>
        <ul class="pop-list">
            <li> 24.2 megapixel APS-C sensor</li>
            <li>45 cross-type AF points </li>
            <li>High-speed continuous shooting at 7fps</li>
            <li>Viewfinder coverage approx. 100%</li>
            <li>HDR Movie / Time-lapse movie</li>
            <li>Wi-Fi/ NFC compatible</li>
        </ul>
    </div>

    <!-- popup--->
    <div class="save-buttons" id="save-btn">
        <a href="http://amsytclients.com/canon/gameplay-photoedit.php" class="btn btn-default">save</a>
        <button type="button" class="btn btn-default">retake</button>
    </div>


    <div class="container-fluid" id="gameplay">  
        <div class="fixed-bottom">
            <img src="assets/images/images/canon.png">
        </div>

    </div>


    <div class="container-fluid" id="instruction">  
        <div class="fixed-bottom">
            <img src="assets/images/images/camera1.png">
        </div>

    </div>
</section>
<script>
    $(document).ready(function () {
        $("#instruction-con").click(function () {
            $("#gameplay").show();
            $("#instruction").hide();
            $(".popup-bg").hide();
            $("#eos-popup").show();

        });
    });

</script>

<script>
    $(document).keypress(function (e) {
        if (e.which == 13) {
            if (($("#popup-bg").css('visibility') === 'hidden') || $('#popup-bg').css('display') == 'none')
            {
                $("#save-btn").show();
                 $("#gameplay").hide();
                   $("#instruction").hide();
                    $(".popup-bg").hide();
                  $("#eos-popup").hide();
            } 
        }
    });

</script>


<?php include 'instruction-footer.php'; ?>