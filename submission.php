<?php include 'register-header.php'; ?>

<section class="submission">
    <div class="container-fluid">
        <div class="row">
         
            <div class="proposal-container proposal-hi" id="proposal-msg">
                    <h4>THE PROPOSAL</h4>
                    <p>This is the exact location I proposed to<br> my wife. It reminds me of where we <br>decided to go hand 
                        in hand on this<br> journey called life. If I won the EOS<br> 80D I would try to recreate some of<br> those
                        special moments I shared with<br> my wife during our travels.</p>
                    <div class="group-btn">
                    <a class="btn btn-default mr-r">Back</a>
                    <a class="btn btn-default" id="proposal-submit">Submit</a>
                </div>
                </div>  
            <div class="proposal-container thank-container" id="thanks-msg">
                    <h4>THANKS FOR<br/> YOUR SUBMISSION</h4>
                    <p class="t-msg">We&acute;ll review and post your<br/> photos to our gallery shortly</p>
                    <div class="group-btn">                    
                        <a class="btn btn-default continue-btn" id="thanks-submit">Continue</a>
                </div>
                </div> 
            
            <div class="proposal-container freeplay-con" id="freeplay-msg">
                    <h4>Oops!<br/>  You&acute;ve exceeded the <br/> submission limit</h4>
                    <p class="t-msg">Feel free to practice using the EOS 80D</p>
                    <div class="group-btn">                    
                     <a class="btn btn-default continue-btn">Continue</a>
                </div>
                </div> 
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function(){
        $("#proposal-submit").click(function(){
         $("#thanks-msg").show();   
         $("#proposal-msg").hide();           
        });
        $("#thanks-submit").click(function(){
         $("#freeplay-msg").show();   
         $("#proposal-msg").hide();  
         $("#thanks-msg").hide();  
        });
    });

</script>



<?php include 'footer.php'; ?>