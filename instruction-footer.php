<section class="instruction-footer">
    <div class="container-fluid">
        
            <div class="col-md-3 col-sm-4 col-xs-12">
                <div class="instruction-footer-left">
                    <ul class="list-unstyled">
                        <li><i class="fa fa-globe"></i><span>back to map</span></li>
                        <li><i><img src="assets/images/photo.svg" class="svg photo"></i><span>Your photos</span></li>
                    </ul>
                </div>
            </div>
        <div class="col-sm-offset-4 col-md-offset-5 col-md-4 col-sm-4 col-xs-12">
            <div class="instruction-footer-right">
                    <ul class="list-inline">
                        <li><i class="navigator"></i></li>
                        <li><i><img src="assets/images/plane.svg" class="svg footer-plane"></i></li>
                    </ul>
                </div>
        </div>       
    </div>
    <div class="clearfix"></div>
        <div class="copyright">
                <div class="col-sm-6 col-xs-12">
                    <ul class="list-inline text-left">
                        <li><a href="http://www.canon.co.in/personal/web/terms" target="_blank">Terms of Use</a></li>
                        <li><a href="http://www.canon.co.in/personal/web/privacy" target="_blank">Privacy Policy</a></li>
                    </ul>
                </div>
                <div class="col-sm-6 col-xs-12 text-right copy-text">
                    <span>Copyright &copy; 2016 Canon India Pvt Ltd. All Rights Reserved</span>
                </div>
                 <div class="clearfix"></div>
            </div>
       
        
        
</section>