<?php include 'map-header.php'; ?>
<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
    <script>	
 
      function init_map() {
		var var_location = new google.maps.LatLng(26.003818,85.081783);
 
        var var_mapoptions = {
          center: var_location,
          zoom: 14
        };
 
		var var_marker = new google.maps.Marker({
			position: var_location,
			map: var_map,
			title:"Venice"});
 
        var var_map = new google.maps.Map(document.getElementById("map-container"),
            var_mapoptions);
 
		var_marker.setMap(var_map);	
 
      }
 
      google.maps.event.addDomListener(window, 'load', init_map);
 
    </script>
<section id="map-container">
    <div class="map-header">
                         <a  href="" class="play-icon"><i class="fa fa-caret-right"></i></a>
                         <a  href=""><i class="fa fa-search search-icon"></i></a>
                    </div>
                     
</section>

<?php include 'map-footer.php'; ?>