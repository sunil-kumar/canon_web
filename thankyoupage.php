<?php include 'register-header.php'; ?>

<section class="submission">
    <div class="container-fluid">
        <div class="row">          
            <div class="proposal-container thank-container" id="thanks-msg">
                    <h4>THANKS FOR<br/> YOUR SUBMISSION</h4>
                    <p class="t-msg">We&acute;ll review and post your<br/> photos to our gallery shortly</p>
                    <div class="group-btn">                    
                     <a class="btn btn-default continue-btn">Continue</a>
                </div>
                </div>               
        </div>
    </div>
    
</section>


<?php include 'footer.php'; ?>