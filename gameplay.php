<?php include 'gameplay-header.php'; ?>
<section class="instruction">
    <div class="ins-popup">
        <img src="assets/images/close.svg" class="svg close-btn">
        <div class="clearfix"></div>
        <h3>Did you know the EOS 80D<br>  is equipped with &ndash;</h3>
        <ul class="pop-list">
            <li> 24.2 megapixel APS-C sensor</li>
            <li>45 cross-type AF points </li>
            <li>High-speed continuous shooting at 7fps</li>
            <li>Viewfinder coverage approx. 100%</li>
            <li>HDR Movie / Time-lapse movie</li>
            <li>Wi-Fi/ NFC compatible</li>
        </ul>
    </div>
    <div class="container-fluid" id="gameplay">  
            <div class="fixed-bottom">
                <img src="assets/images/images/canon.png">
            </div>
        
    </div>
</section>

<?php include 'instruction-footer.php'; ?>