<?php include 'register-header.php'; ?>
<link rel="stylesheet" href="assets/css/bootstrap-slider.css">
<link rel="stylesheet" href="assets/css/bootstrap-slider.min.css">
<script src="assets/js/bootstrap-slider.js"></script>
<script src="assets/js/bootstrap-slider.min.js"></script>

<section class="edit">
    <div class="container-fluid">
    <div class="row">
        <div class="col-md-8 padding-0">
            <div class="view-image">  
                
            </div>
        </div>
        <div class="col-md-4 padding-0">
            <div class="edit-instruction">
                <form class="form-horizontal edit-form" role="form">
                        <div class="form-group">
                          <label class="control-label col-sm-3 col-xs-2 col-lg-3 text-uppercase">Title</label>
                          <div class=" col-xs-10 col-sm-9 col-lg-9">
                            <input type="email" class="form-control textbox-bg" id="email" placeholder="Please fill in">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-xs-2 col-sm-3 col-lg-3 text-uppercase">Caption</label>
                          <div class="col-xs-10 col-sm-9 col-lg-9"> 
                              <textarea class="form-control textbox-bg ff-font" rows="4">Describe your submission and&#13;&#10;tell us why do you want the&#13;&#10;EOS 8</textarea>
                          </div>
                        </div>
                </form>
                <form role="form" class="slider-form">
                        <div class="form-group">
                          <label class="control-label col-md-12 text-uppercase">Fisheye </label>
                          <input id="ex1" data-slider-id='ex1Slider' type="text" data-slider-min="0" data-slider-max="100" data-slider-step="1" data-slider-value="50"/>         
                     </div>
                        <div class="form-group">
                          <label class="control-label col-md-12 text-uppercase">Monochrome </label>
                          <input id="ex2" data-slider-id='ex1Slider' type="text" data-slider-min="0" data-slider-max="100" data-slider-step="1" data-slider-value="50"/>         
                     </div>
                        <div class="form-group">
                          <label class="control-label col-md-12 text-uppercase">TILT-SHIFT </label>
                          <input id="ex3" data-slider-id='ex1Slider' type="text" data-slider-min="0" data-slider-max="100" data-slider-step="1" data-slider-value="50"/>         
                     </div>
                    <div class="form-group">
                          <label class="control-label col-md-12 text-uppercase">IMage Sharpness </label>
                          <input id="ex4" data-slider-id='ex1Slider' type="text" data-slider-min="0" data-slider-max="100" data-slider-step="1" data-slider-value="50"/>         
                     </div>
                    <div class="text-right">
                        <a href="http://amsytclients.com/canon/submission.php" class="btn btn-default savephoto-btn pull-right" >SAVE PHOTO</a>
                    </div>
                </form>
                    
                
                <script>
                $('#ex1,#ex2,#ex3,#ex4').slider({
	formatter: function(value) {
		return 'Current value: ' + value;
	}
});

                </script>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    </div>
    
</section>
<section class="image-group">
    <div>
        <div class="side-image">
            <img src="assets/images/images/vt11.png" class="img-responsive"> 
            <div class="overlay">
                <img src="assets/images/close.svg" class="svg close-btn overlay-cross-btn">
            </div>
        </div>
        <div class="side-image">
            <img src="assets/images/images/vt22.png" class="img-responsive">
            <div class="overlay">
                 <img src="assets/images/close.svg" class="svg close-btn overlay-cross-btn">
            </div>
        </div>
        <div class="side-image">
            <img src="assets/images/images/vt33.png" class="img-responsive"> 
            <div class="overlay">
                <img src="assets/images/close.svg" class="svg close-btn overlay-cross-btn">
            </div>
        </div>
        <div class="side-image">
            <img src="assets/images/images/vt44.png" class="img-responsive"> 
            <div class="overlay">
                <img src="assets/images/close.svg" class="svg close-btn overlay-cross-btn">
            </div>
        </div>
        <div class="side-image">
            <img src="assets/images/images/vt55.png" class="img-responsive"> 
            <div class="overlay">
                 <img src="assets/images/close.svg" class="svg close-btn overlay-cross-btn">
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row edit-footer">
        <div class="col-md-7 col-sm-7">
            <p>Choose up to 3 of your favourite entries to submit everyday.</p>
        </div>
        <div class="col-md-5 col-sm-5">
            <div class="edit-footer-right text-right">
            <ul class="list-inline">
                <li><i class="fa fa-globe"></i><span>back to map</span></li>
                <li><div class="group-btn"><button type="button" class="btn btn-default">Submit</button></div></li>
            </ul>
        </div>
        </div>
    </div>
    
</section>
<div class="clearfix"></div>
<div class="photoedit-footer">
        <div class="copyright">
                <div class="col-sm-6">
                    <ul class="list-inline text-left">
                        <li><a href="http://www.canon.co.in/personal/web/terms" target="_blank">Terms of Use</a></li>
                        <li><a href="http://www.canon.co.in/personal/web/privacy" target="_blank">Privacy Policy</a></li>
                    </ul>
                </div>
                <div class="col-md-6 text-right copy-text">
                    <span>Copyright &copy; 2016 Canon India Pvt Ltd. All Rights Reserved</span>
                </div>
                 <div class="clearfix"></div>
            </div>
</div>