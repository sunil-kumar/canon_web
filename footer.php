<div class="clearfix"></div>
    <footer>
        <div class="container-fluid">
        <div class="row">
    <div class="col-md-12">
        <div class="bottom-fixed-footer">
            <ul class="list-inline flight-book">
                <li><img src="assets/images/plane.svg"  class="plain svg" /> <span>Book flights with</span></li>
                <li><img src="assets/images/images/ana_logo.png" width="100"/></li>             
            </ul>
            <div class="clearfix"></div>
            <div class="copyright">
                <div class="col-sm-6 col-xs-12">
                    <ul class="list-inline text-left link-text">
                        <li><a href="http://www.canon.co.in/personal/web/terms" target="_blank">Terms of Use</a></li>
                        <li><a href="http://www.canon.co.in/personal/web/privacy" target="_blank">Privacy Policy</a></li>
                    </ul>
                </div>
                <div class="col-sm-6 col-xs-12 text-right copy-text">
                    <span>Copyright &copy; 2016 Canon India Pvt Ltd. All Rights Reserved</span>
                </div>
                 <div class="clearfix"></div>
            </div>
        </div>
    </div>
    </div>
        </div>
    </footer>